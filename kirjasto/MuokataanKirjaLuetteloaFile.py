#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# MuokataanKirjaLuetteloaFile.py

import os
from ColorFile import *
from TulostaKirjaLuetteloFile import *


def ShowMessage(msg):
	os.system('zenity --info --title "HUOM" --width=200 --height=100 --text "%s"' %msg)


def MuokataanKirjaLuetteloa(List):
	data=""
	Num=1
	for key in List:
		data = data + ' "'+str(Num)+'" '
		data = data + ' " '+key["kirja"]+' " '
		data = data + ' " '+key["kirjailija"]+' " '
		data = data + ' " '+key["jv"]+'" '
		Num  = Num  + 1

	if len(data) >= 6:
		if os.path.isfile('muokattava'):
			os.rename('muokattava', 'muokattava.bak')

		data ='zenity --width="350" --height="450" --editable --mid-search --separator="\n" --list --hide-column="1"  --print-column="ALL" --text="\t\t*** H U O M I O I T H A N   T Ä M Ä N ***\n\n1.)  -Voit muokata vaikka jokaista tietuetta, mutta vain\n\tviimmeisen tietueen muokkaus toteutetaan.\n2.)  -Sarakkeiden otsikoista aakkostus, jota ei\n\tkuitenkaan tallenneta.\n" --title="Valitse poistettava" --column="x" --column="Kirja" --column="Kirjoittaja" --column="Vuosi"' + data + ' > muokattava'
		os.system(data)

		
		MuokattavanSize = os.path.getsize("muokattava")
		if MuokattavanSize > 6:
			kirja=""
			with open('muokattava') as f:
				Num = f.readline().strip()
				kirja = f.readline().strip()
				kirjailija = f.readline().strip()
				jv = f.readline().strip()
				
				
			if kirja != "" and kirjailija != "" and jv != "":
				MuokattavaKirja = {"kirja":kirja, "kirjailija":kirjailija, "jv":jv}
				List[int(Num) - 1] = MuokattavaKirja
				TulostaKirjaLuettelo(List)
