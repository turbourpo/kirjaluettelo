#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# PoistaKohdeKirjaLuettelostaFile.py

import os
from ColorFile import *
from TulostaKirjaLuetteloFile import *

def ShowMessage(msg):
	os.system('zenity --info --title "HUOM" --width=200 --height=100 --text "%s"' %msg)


def PoistaKohdeKirjaLuettelosta(List):
	data=""
	for key in List:
		data = data + ' " " '
		data = data + ' "'+key["kirja"]+'" '
		data = data + ' "'+key["kirjailija"]+'" '
		data = data + ' "'+key["jv"]+'" '

	if len(data) >= 6:
		if os.path.isfile('poistettava'):
			os.rename('poistettava', 'poistettava.bak')

		data ='zenity --width="400" --height="450" --separator="\n" --list --radiolist --print-column="ALL" --title="Valitse poistettava" --column="x" --column="Kirja" --column="Kirjoittaja" --column="Vuosi"' + data + ' > poistettava'
		os.system(data)
		
		PoistettavaSize = os.path.getsize("poistettava")
		if PoistettavaSize > 6:
			kirja=""
			with open('poistettava') as f:
				kirja = f.readline().strip()
				kirjailija = f.readline().strip()
				jv = f.readline().strip()
			if kirja != "" and kirjailija != "" and jv != "":
				PoistettavaKirja = {"kirja":kirja, "kirjailija":kirjailija, "jv":jv}
				for i in range(len(List)):
					if List[i] == PoistettavaKirja:
						del List[i]
						TulostaKirjaLuettelo(List)
						break
