#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# TulostaKirjaLuetteloFile.py

import os
from ColorFile import *

def ShowMessage(msg):
	os.system('zenity --info --title "HUOM" --width=200 --height=100 --text "%s"' %msg)


def TulostaKirjaLuettelo(List):
	print(CRED + '\n\tKIRJA                   KIRJOITTAJA           VUOSI' + CEND)
	if not List:
		msg="Kirjaluetteloa ei ole olemassa, tai se on tyhjä."
		ShowMessage(msg)
	else:
		for key in List:
			data = "\t"+key["kirja"]
			data = "{:<25}".format(data)
			data = data + key["kirjailija"]
			data = "{:<47}".format(data)
			data = data + key["jv"]
			print(data)
	input(CGREEN + '\n\n\tPaina jotain jatkaaksesi! ' + CEND)
