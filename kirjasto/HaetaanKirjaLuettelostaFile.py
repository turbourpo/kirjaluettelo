#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# HaetaanKirjaLuettelostaFile.py

import os
from ColorFile import *
from TulostaKirjaLuetteloFile import *


def ShowMessage(msg):
	print(msg)
	input(CGREEN + '\n\n\tPaina jotain jatkaaksesi! ' + CEND)




def HaeKirjanNimella(Lista):
	os.system('zenity --entry --width="250" --title "Tarvitaan nimi" --text "Haetaan kirjaa nimellä. Haku palauttaa kaikki ne kirjat,\n joiden nimessä esiintyy antamasi merkkijono\n\nKirjoita kirjan nimi tai osa siitä:" > result')
	ResultSize = os.path.getsize("result")
	if ResultSize > 1:
		with open('result') as f:
			BookName = f.readline().strip()
			FoundList=[]
			for key in Lista:
				if BookName in key["kirja"]:
					kirja = key["kirja"]
					kirjailija = key["kirjailija"]
					jv = key["jv"]
					uusiKirja = {"kirja":kirja, "kirjailija":kirjailija, "jv":jv}
					FoundList.append(uusiKirja)
			TulostaKirjaLuettelo(FoundList)

	


def HaeKirjoittajanNimella(Lista):
	os.system('zenity --entry --width="250" --title "Tarvitaan nimi" --text "Haetaan kirjaa kirjailijan nimellä. Haku palauttaa kaikki ne kirjat,\n joiden nimessä esiintyy antamasi merkkijono\n\nKirjoita kirjailian nimi tai osa siitä:" > result')
	ResultSize = os.path.getsize("result")
	if ResultSize > 1:
		with open('result') as f:
			AuthorName = f.readline().strip()
			FoundList=[]
			for key in Lista:
				if AuthorName in key["kirjailija"]:
					kirja = key["kirja"]
					kirjailija = key["kirjailija"]
					jv = key["jv"]
					uusiKirja = {"kirja":kirja, "kirjailija":kirjailija, "jv":jv}
					FoundList.append(uusiKirja)
			TulostaKirjaLuettelo(FoundList)




def HaeVuosiluvulla(Lista):
	os.system('zenity --entry --width="250" --title "Tarvitaan vuosiluku" --text "Haetaan kirjaa vuosiluvun perusteella. Haku palauttaa kaikki ne kirjat,\n joiden julkaisuvuodessa esiintyy antamasi merkkijono\n\nKirjoita vuosiluku tai osa siitä:" > result')
	ResultSize = os.path.getsize("result")
	if ResultSize > 1:
		with open('result') as f:
			Year = f.readline().strip()
			FoundList=[]
			for key in Lista:
				if Year in key["jv"]:
					kirja = key["kirja"]
					kirjailija = key["kirjailija"]
					jv = key["jv"]
					uusiKirja = {"kirja":kirja, "kirjailija":kirjailija, "jv":jv}
					FoundList.append(uusiKirja)
			TulostaKirjaLuettelo(FoundList)
