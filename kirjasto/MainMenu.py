#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# MainMenu.py

import os
from ColorFile import *
from LataaKirjaLuetteloFile import *
from HaetaanKirjaLuettelostaFile import *
from MuokataanKirjaLuetteloaFile import *
from PoistaKohdeKirjaLuettelostaFile import *
from UusiKohdeKirjaLuetteloonFile import *
from TulostaKirjaLuetteloFile import *
from TallennaKirjaLuetteloFile import *


FileName='Kirjasto.dat'

def HaeMenu():
	while True:
		os.system('clear') 
		print ("\n\n")
		print ("\t" + CGREEN + "K I R J A L U E T T E L O S T A  H A K U" + CEND)
		print (CRED + CBOLD + "\t1." + CEND + " Hae kirjan nimellä.")
		print (CRED + CBOLD + "\t2." + CEND + " Hae kirjailian nimellä.")
		print (CRED + CBOLD + "\t3." + CEND + " Hae kirjan julkaisuvuoden perusteella.")
		print (CRED + CBOLD + "\t4." + CEND + " Lopetus")
		
		valinta = input('\n\t   Anna valintasi ['+CRED+CBOLD+'1-4'+CEND+'] : ')
		if valinta 	 == "1": os.system('clear'); HaeKirjanNimella(Lista)
		elif valinta == "2": os.system('clear'); HaeKirjoittajanNimella(Lista)
		elif valinta == "3": os.system('clear'); HaeVuosiluvulla(Lista)
		elif valinta == "4": break



def MainMenu():
	global Lista
	Lista=LataaKirjaLuettelo(FileName)
	while True:
		os.system('clear') 
		print ("\n\n")
		print ("\t" + CGREEN + "K I R J A L U E T T E L O N  P Ä Ä V A L I K K O" + CEND)
		print (CRED + CBOLD + "\t1." + CEND + " Hae kirjan/kirjailijan nimellä tai vuosiluvulla.")
		print (CRED + CBOLD + "\t2." + CEND + " Muokkaa kirjaluettelon merkintää.")
		print (CRED + CBOLD + "\t3." + CEND + " Poista kirjaluettelon merkintä.")
		print (CRED + CBOLD + "\t4." + CEND + " Lisää kirjaluetteloon merkintä.")
		print (CRED + CBOLD + "\t5." + CEND + " Tulosta kirjaluettelo.")
		print (CRED + CBOLD + "\t6." + CEND + " Tallenna kirjaluettelo.")
		print (CRED + CBOLD + "\t7." + CEND + " Lopetus")
		
		valinta = input('\n\t   Anna valintasi ['+CRED+CBOLD+'1-7'+CEND+'] : ')
		if valinta 	 == "1": os.system('clear'); HaeMenu()
		elif valinta == "2": os.system('clear'); MuokataanKirjaLuetteloa(Lista)
		elif valinta == "3": os.system('clear'); PoistaKohdeKirjaLuettelosta(Lista)
		elif valinta == "4": os.system('clear'); UusiKohdeKirjaLuetteloon(Lista)
		elif valinta == "5": os.system('clear'); TulostaKirjaLuettelo(Lista)
		elif valinta == "6": os.system('clear'); TallennaKirjaLuettelo(Lista,FileName)
		elif valinta == "7": break
	
MainMenu()
