#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# TallennaKirjaLuetteloFile.py

import json
import os
from ColorFile import *


def ShowMessage(msg):
	os.system('zenity --info --title "HUOM" --width=200 --height=100 --text "%s"' %msg)


def TallennaKirjaLuettelo(List, FileName):
	if not List:
		msg="Kirjaluettelo on tyhjä!\n\nLisää luetteloon ensin jotain, ja tallenna vasta sitten."
		ShowMessage(msg)
	elif not FileName:
		msg="Ongelmia tiedostonimen kanssa!\n\nTarkista että kutsut tallenusfuktiota tiedostonimellä varustettuna."
		ShowMessage(msg)
	else:
		if os.path.isfile(FileName):
			os.rename(FileName, FileName+'.bak')
			jsonString = json.dumps(List)
			jsonFile = open(FileName, "w")
			jsonFile.write(jsonString)
			jsonFile.close()
			msg="Olemassa ollut (" + FileName + ") tiedosto, nimetty (" + FileName + ".bak).  \n\n"
			msg=msg+"Uusi kirjaluettelo tallennettu nimellä (" + FileName + ") onnistuneesti.  "
			ShowMessage(msg)
		else:
			with open(FileName, 'w+b') as f:
				pickle.dump(List, f)
				f.close()
				msg="Uusi kirjaluettelo tallennettu nimellä (" + FileName + ") onnistuneesti.  "
				ShowMessage(msg)
