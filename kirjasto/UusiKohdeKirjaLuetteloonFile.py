#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# UusiKohdeKirjaLuetteloonFile.py

import os
from ColorFile import *
from TulostaKirjaLuetteloFile import *


def UusiKohdeKirjaLuetteloon(List):
	if os.path.isfile('result'):
		os.rename('result', 'result.bak')

	os.system('zenity --forms --separator="\n" --title="Lisää uusi kirja luetteloon" --text="Kirjan tiedot" --add-entry="Kirjan nimi" --add-entry="Kirjailijan nimi" --add-entry="Julkaisu vuosi" > result')
	ResultSize = os.path.getsize("result")
	if ResultSize > 6:
		with open('result') as f:
			kirja = f.readline().strip()
			kirjailija = f.readline().strip()
			jv = f.readline().strip()
			if kirja != "" and kirjailija != "" and jv != "":
				uusiKirja = {"kirja":kirja, "kirjailija":kirjailija, "jv":jv}
				List.append(uusiKirja)
				TulostaKirjaLuettelo(List)
